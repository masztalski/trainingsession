package io.fantom.trainingsession.events;

public class DeleteProfileEvent {
    private int mProfileID;

    public DeleteProfileEvent(int profileID) {
        mProfileID = profileID;
    }

    public int getProfileID() {
        return mProfileID;
    }
}
