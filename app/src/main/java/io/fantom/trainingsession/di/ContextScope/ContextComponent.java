package io.fantom.trainingsession.di.ContextScope;

import dagger.Subcomponent;
import io.fantom.trainingsession.activities.MainActivity;
import io.fantom.trainingsession.activities.TrainingActivity;
import io.fantom.trainingsession.dialogs.CreateProfileDialog;
import io.fantom.trainingsession.dialogs.DeleteProfileDialog;

@ContextScope
@Subcomponent
public interface ContextComponent {
    void inject (MainActivity activity);
    void inject (TrainingActivity activity);

    void inject (CreateProfileDialog dialog);
    void inject (DeleteProfileDialog dialog);
}
