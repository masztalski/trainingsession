package io.fantom.trainingsession.di;

import javax.inject.Singleton;

import dagger.Component;
import io.fantom.trainingsession.di.ContextScope.ContextComponent;
import io.fantom.trainingsession.di.module.RepositoryModule;

@Singleton
@Component(modules = {ApplicationModule.class, RepositoryModule.class})
public interface ApplicationComponent {
    ContextComponent newContextComponent();
}
