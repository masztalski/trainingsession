package io.fantom.trainingsession.di.module;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.fantom.trainingsession.repository.DatabaseHelper;
import io.fantom.trainingsession.repository.TrainingProfileRepository;
import io.fantom.trainingsession.repository.TrainingProfileSQLImpl;

@Module
public class RepositoryModule {

    @Singleton
    @Provides
    public ConnectionSource providesConnectionSource(Application app){
        return new AndroidConnectionSource(new DatabaseHelper(app));
    }
    @Singleton
    @Provides
    public TrainingProfileRepository providesTrainingProfileRepository(ConnectionSource connectionSource){
        return new TrainingProfileSQLImpl(connectionSource);
    }

}
