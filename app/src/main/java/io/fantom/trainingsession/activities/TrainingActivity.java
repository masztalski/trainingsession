package io.fantom.trainingsession.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import javax.inject.Inject;

import io.fantom.trainingsession.R;
import io.fantom.trainingsession.TrainingApplication;
import io.fantom.trainingsession.databinding.ActivityTrainingBinding;
import io.fantom.trainingsession.views.CountDownTimerView;

public class TrainingActivity extends AppCompatActivity {
    public static final String PROFILE_ID = "profile_id";

    private ActivityTrainingBinding mBinding;

    @Inject
    TrainingController mController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_training);

        ((TrainingApplication) getApplication())
                .mApplicationComponent
                .newContextComponent()
                .inject(this);

        if (getIntent().getExtras() != null && getIntent().hasExtra(PROFILE_ID)) {
            mController.loadProfile(getIntent().getIntExtra(PROFILE_ID, -1));
        }

        mBinding.setCurrentRepeats(mController.getCurrentRepeats());
        mBinding.setCurrentSeries(mController.getCurrentSeries());

        mController.setMediaPlayer(this);
        mController.setVibrator(this);

        mController.subscribeToList(profile -> {
            //Training start
            mBinding.setProfile(profile);
            mBinding.setRepeatsRest(true);
            mBinding.counter.restFinish();
        });

        mBinding.parent.setOnClickListener(v -> {
            if (mBinding.restButton.isEnabled()) {
                mController.stopSound();
                mController.stopVibrate();
            }
        });

        mBinding.counter.setCountDownCallback(new CountDownTimerView.CountDownCallback() {
            @Override
            public void onCountDownFinished() {
                mBinding.progressIndicator.setProgress(mBinding.progressIndicator.getMax());
                mBinding.restButton.setEnabled(true);
                mController.playSound();
                mController.startVibrate();
                Log.i("TIME", "counting finished");
            }

            @Override
            public void onTick(int secondsUntilFinished) {
                mBinding.progressIndicator.setProgress(mBinding.progressIndicator.getMax() - secondsUntilFinished);
            }
        });

        mBinding.finishButton.setOnClickListener(view -> finish());

        mBinding.restButton.setOnClickListener(view -> {
            mController.setCurrentRepeats(mController.getCurrentRepeats() + 1);
            mBinding.repats.addView(addBadge());

            boolean seriaCompleted = mController.getCurrentRepeats() == mBinding.getProfile().getRepeats(); //czy uko�czy�em seri�?

            // Uko�czone powt�rzenie -> odpoczywam
            boolean treningCompleted = false;

            if (seriaCompleted) {
                mController.setCurrentRepeats(0);
                mBinding.setCurrentRepeats(0);

                mController.setCurrentSeries(mController.getCurrentSeries() + 1);
                mBinding.setCurrentSeries(mController.getCurrentSeries());
                Toast.makeText(TrainingActivity.this, "Uko�czona seria, odpoczynek", Toast.LENGTH_SHORT).show();
                treningCompleted = mController.getCurrentSeries() == mBinding.getProfile().getSeries();
                if (treningCompleted) {
                    Toast.makeText(this, "Trening uko�czony :)", Toast.LENGTH_LONG).show();
                    mBinding.restButton.setVisibility(View.GONE);
                    mBinding.finishButton.setVisibility(View.VISIBLE);
                }

                mBinding.series.addView(addBadge());
                mBinding.repats.removeViews(1, mBinding.repats.getChildCount() - 1);
            }


            mBinding.setCurrentRepeats(mController.getCurrentRepeats());

            mBinding.setRepeatsRest(!seriaCompleted); // nie uko�czona - rest powt�rze�

            if (!treningCompleted){
                if (seriaCompleted) {
                    mBinding.counter.startTimer(mBinding.getProfile().getSeriesRest()*60);
                } else {
                    mBinding.counter.startTimer(mBinding.getProfile().getRepeatsRest()*60 + mBinding.getProfile().getRepeatsRestSec());
                }

            }

            mBinding.restButton.setEnabled(false);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBinding.counter.stopCountDown();
        mController.stopSound();
        mController.stopVibrate();
    }

    private ImageView addBadge() {
        ImageView badge = new ImageView(this);
        badge.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fire_black_24dp));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        badge.setLayoutParams(params);
        badge.setPadding(0, 8, 0, 0);
        Animation stampAnimation = AnimationUtils.loadAnimation(this, R.anim.stamp_anim);
        badge.startAnimation(stampAnimation);
        return badge;
    }
}
