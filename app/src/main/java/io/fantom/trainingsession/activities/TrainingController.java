package io.fantom.trainingsession.activities;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Vibrator;

import java.io.IOException;

import javax.inject.Inject;

import io.fantom.trainingsession.models.TrainingProfile;
import io.fantom.trainingsession.repository.TrainingProfileRepository;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.ReplaySubject;

public class TrainingController {

    @Inject
    TrainingProfileRepository mTrainingProfileRepository;

    private final ReplaySubject<TrainingProfile> mProfileSubject = ReplaySubject.createWithSize(1);

    private int mCurrentRepeats = 0;
    private int mCurrentSeries = 0;

    private MediaPlayer mMediaPlayer;
    private CountDownTimer mPlayerTimer;
    private Vibrator mVibrator;

    @Inject
    public TrainingController() {
    }

    public void loadProfile(int profileID) {
        if (profileID != -1) {
            mProfileSubject.onNext(mTrainingProfileRepository.findById(profileID));
            mProfileSubject.onComplete();
        }
    }

    public Disposable subscribeToList(Consumer<TrainingProfile> callback) {
        return mProfileSubject != null ? mProfileSubject.subscribe(callback) : null;
    }

    public int getCurrentRepeats() {
        return mCurrentRepeats;
    }

    public void setCurrentRepeats(int currentRepeats) {
        mCurrentRepeats = currentRepeats;
    }

    public int getCurrentSeries() {
        return mCurrentSeries;
    }

    public void setCurrentSeries(int currentSeries) {
        mCurrentSeries = currentSeries;
    }

    public void setMediaPlayer(Context context) {
        try {
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(context, soundUri);
            final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

            mPlayerTimer = new CountDownTimer(30000, 1000) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    mMediaPlayer.stop();
                }
            };

            if (audioManager != null && audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.setOnPreparedListener(mp -> {
                    mMediaPlayer.start();
                });

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playSound() {
        try {
            mMediaPlayer.prepare();
            mPlayerTimer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopSound() {
        mPlayerTimer.onFinish();
    }

    public void setVibrator(Context context) {
        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public void startVibrate() {
        mVibrator.vibrate(10000);
    }

    public void stopVibrate() {
        if (mVibrator != null) {
            mVibrator.cancel();
        }
    }
}
