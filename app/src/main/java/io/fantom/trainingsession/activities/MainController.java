package io.fantom.trainingsession.activities;

import java.util.List;

import javax.inject.Inject;

import io.fantom.trainingsession.models.TrainingProfile;
import io.fantom.trainingsession.repository.TrainingProfileRepository;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.ReplaySubject;

public class MainController{

    @Inject
    TrainingProfileRepository mTrainingProfileRepository;

    private final ReplaySubject<List<TrainingProfile>> mProfilesSubject = ReplaySubject.createWithSize(1);

    @Inject
    public MainController(){
    }

    public void loadData(){
        mProfilesSubject.onNext(mTrainingProfileRepository.findAll());
        mProfilesSubject.onComplete();
    }

    public Disposable subscribeToList(Consumer<List<TrainingProfile>> callback) {
        return mProfilesSubject != null ? mProfilesSubject.subscribe(callback) : null;
    }

}
