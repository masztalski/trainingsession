package io.fantom.trainingsession.activities;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import javax.inject.Inject;

import io.fantom.trainingsession.R;
import io.fantom.trainingsession.TrainingApplication;
import io.fantom.trainingsession.adapters.ButtonHandler;
import io.fantom.trainingsession.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    @Inject
    protected MainController mController;

    private ActivityMainBinding mBinding;
    private ButtonHandler mButtonHandler;
    private ObservableArrayList mProfilesList = new ObservableArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(mBinding.toolbar);
        mButtonHandler = new ButtonHandler(mProfilesList);
        mBinding.setProfilesList(mProfilesList);
        mButtonHandler.setMainActivityContext(this);

        mBinding.setHandler(mButtonHandler);

        ((TrainingApplication) getApplication())
                .mApplicationComponent
                .newContextComponent()
                .inject(this);

        mController.loadData();

        mController.subscribeToList(trainingProfiles -> mProfilesList.addAll(trainingProfiles));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
