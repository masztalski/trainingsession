package io.fantom.trainingsession.views;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import io.fantom.trainingsession.models.TrainingProfile;
import io.fantom.trainingsession.utils.TextFormatUtils;

public class CountDownTimerView extends AppCompatTextView {

    public interface CountDownCallback {
        void onCountDownFinished();

        void onTick(int secondsUntilFinished);
    }

    private CountDownTimer mCountDownTimer;
    private CountDownCallback mCountDownCallback;
    private float mDefaultTextSize;
    private int leftTime = 0;

    public CountDownTimerView(Context context) {
        super(context);
        mDefaultTextSize = getTextSize();
    }

    public CountDownTimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mDefaultTextSize = getTextSize();
    }

    public CountDownTimerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mDefaultTextSize = getTextSize();
    }

    public void setCountDownCallback(CountDownCallback countDownCallback) {
        mCountDownCallback = countDownCallback;
    }

    /**
     * Metoda konfigurująca timer. Obsługuje wyświetlanie pozostałego czasu oraz tworzy dialog z
     * informacją o przekroczeniu czasu wypełniania raportu
     *
     */
    public void startTimer(int timeSeconds) {
        CountDownTimerView.this.setTextSize(mDefaultTextSize);
        CountDownTimerView.this.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));

        mCountDownTimer = new CountDownTimer(timeSeconds * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long minutes = millisUntilFinished / (1000 * 60);
                long seconds = (millisUntilFinished % (1000 * 60)) / 1000;

                leftTime = (int) (millisUntilFinished / 1000);

                CountDownTimerView.this.setText(TextFormatUtils.formatTimeCounter(minutes, seconds));
                if (mCountDownCallback != null) mCountDownCallback.onTick((int)(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                restFinish();
                mCountDownTimer.cancel();
                if (mCountDownCallback != null) mCountDownCallback.onCountDownFinished();
            }
        }.start();
    }

    public void stopCountDown() {
        if(mCountDownTimer != null) mCountDownTimer.cancel();
    }

    public void restFinish(){
        CountDownTimerView.this.setText("GO!!!");
        CountDownTimerView.this.setTextSize(20);
        CountDownTimerView.this.setTextColor(ContextCompat.getColor(getContext(),android.R.color.holo_red_dark));
    }

    public int getLeftTime(){
        return leftTime;
    }
}
