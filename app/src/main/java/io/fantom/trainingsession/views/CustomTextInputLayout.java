package io.fantom.trainingsession.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.databinding.InverseBindingListener;
import android.databinding.InverseBindingMethod;
import android.databinding.InverseBindingMethods;
import android.databinding.adapters.ListenerUtil;
import android.databinding.adapters.TextViewBindingAdapter;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import io.fantom.trainingsession.R;

@InverseBindingMethods({
        @InverseBindingMethod(type = CustomTextInputLayout.class, attribute = "app:numberValue", method = "getNumber", event = "android:numberValueAttrChanged"),
})
public class CustomTextInputLayout extends TextInputLayout {

    public CustomTextInputLayout(Context context) {
        super(context);
        init(context, null);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @Nullable
    public String getText() {
        if (getEditText() == null || getEditText().getText() == null)
            return null;
        return !getEditText().getText().toString().isEmpty() ? getEditText().getText().toString() : null;
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.custom_input_layout, this);

        if (attrs == null)
            return;

        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.CustomTextInputLayout, 0, 0);

        String hintText, text;
        int inputType = EditorInfo.TYPE_NULL;
        try {
            text = typedArray.getString(R.styleable.CustomTextInputLayout_CTILText);
            hintText = typedArray.getString(R.styleable.CustomTextInputLayout_CTILHintText);
            inputType = typedArray.getInt(R.styleable.CustomTextInputLayout_android_inputType, EditorInfo.TYPE_NULL);

            int textSize = typedArray.getDimensionPixelSize(R.styleable.CustomTextInputLayout_android_textSize, 0);
            if (textSize > 0 && getEditText() != null) {
                getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            }
        } finally {
            typedArray.recycle();
        }

        setTextValue(text);
        setHintText(hintText);
        setInputType(inputType);
    }

    private void setTextValue(@Nullable String text) {
        if (getEditText() != null) {
            if (text == null || text.isEmpty())
                text = "";
            getEditText().setText(text);
        }
    }

    private void setHintText(@Nullable String hintText) {
        if (hintText == null || hintText.isEmpty())
            hintText = "";
        this.setHint(hintText);
    }

    private void setInputType(int inputType) {
        if (getEditText() != null) {
            getEditText().setInputType(inputType);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (getEditText() != null) getEditText().setEnabled(enabled);
    }

    public Integer getNumber(){
        return this.getText() != null ? Integer.valueOf(this.getText()) : null;
    }

    @BindingAdapter("app:numberValue")
    public static void setNumber(CustomTextInputLayout view, Integer text){
        Log.i("SetNumber", "ID: " + view.getId() + " Value: " + (text != null ? text.toString() : ""));
        if (text != null) {
            view.setTextValue(text.toString());
        }
    }

    @BindingAdapter(value = {"android:afterTextChanged", "android:numberValueAttrChanged",},
            requireAll = false)
    public static void setTextWatcher(CustomTextInputLayout view,
                                      final TextViewBindingAdapter.AfterTextChanged after,
                                      final InverseBindingListener numberAttrChanged) {
        TextWatcher newValue = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.i("Watcher", "After text changed: " + editable.toString());
                if (after != null) {
                    after.afterTextChanged(editable);
                }
                if (numberAttrChanged != null) {
                    numberAttrChanged.onChange();
                }
            }
        };
        TextWatcher oldValue = ListenerUtil.trackListener(view.getEditText(), newValue, R.id.textWatcher);
        if (oldValue != null) {
            view.getEditText().removeTextChangedListener(oldValue);
        }
        view.getEditText().addTextChangedListener(newValue);
    }

}
