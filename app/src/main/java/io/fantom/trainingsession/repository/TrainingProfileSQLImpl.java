package io.fantom.trainingsession.repository;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import io.fantom.trainingsession.models.TrainingProfile;

public class TrainingProfileSQLImpl implements TrainingProfileRepository {
    protected Dao<TrainingProfile,Integer> mDao;

    public TrainingProfileSQLImpl(ConnectionSource connectionSource){
        try {
            mDao = DaoManager.createDao(connectionSource, TrainingProfile.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(TrainingProfile profile) {
        try {
            mDao.create(profile);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<TrainingProfile> findAll() {
        try {
            return mDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public TrainingProfile findById(int profileID) {
        try {
            return mDao.queryForId(profileID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteAll() {
        try {
            mDao.delete(findAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int profileID) {
        try {
            TrainingProfile profile = findById(profileID);
            if (profile != null) mDao.delete(profile);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
