package io.fantom.trainingsession.repository;

import java.util.List;

import io.fantom.trainingsession.models.TrainingProfile;

public interface TrainingProfileRepository {
    List<TrainingProfile> findAll();
    TrainingProfile findById(int profileID);

    void deleteAll();
    void delete(int profileID);

    void create(TrainingProfile profile);
}
