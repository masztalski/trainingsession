package io.fantom.trainingsession.utils;

public class TextFormatUtils {
    public static String formatTimeCounter(long minutes, long seconds) {
        return String.format("%02d", minutes) + " : " + String.format("%02d", seconds);
    }
}
