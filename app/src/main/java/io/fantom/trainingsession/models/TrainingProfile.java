package io.fantom.trainingsession.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import io.fantom.trainingsession.BR;

@DatabaseTable(tableName = "trainingProfile")
public class TrainingProfile extends BaseObservable {
    @DatabaseField(generatedId = true)
    private int mID;
    @DatabaseField
    private Integer mSeries;
    @DatabaseField
    private Integer mRepeats;
    @DatabaseField
    private Integer mSeriesRest; //minutes
    @DatabaseField
    private Integer mSeriesRestSec; //seconds
    @DatabaseField
    private Integer mRepeatsRest; //minutes
    @DatabaseField
    private Integer mRepeatsRestSec; //seconds

    public TrainingProfile(){
        mSeries = 1;
        mRepeats = 1;
        mSeries = 1;
        mSeriesRest = 1;
        mSeriesRestSec = 0;
        mRepeatsRest = 1;
        mRepeatsRestSec = 0;
    }

    public TrainingProfile(Integer series, Integer repeats, Integer seriesRest, Integer seriesRestSec, Integer repeatsRest, Integer repeatsRestSec) {
        mSeries = series;
        mRepeats = repeats;
        mSeriesRest = seriesRest;
        mSeriesRestSec = seriesRestSec;
        mRepeatsRest = repeatsRest;
        mRepeatsRestSec = repeatsRestSec;
    }

    @Bindable
    public Integer getSeries() {
        return mSeries;
    }

    public void setSeries(Integer series) {
        mSeries = series;
//        notifyPropertyChanged(BR.series);
    }

    @Bindable
    public Integer getRepeats() {
        return mRepeats;
    }

    public void setRepeats(Integer repeats) {
        mRepeats = repeats;
//        notifyPropertyChanged(BR.repeats);
    }

    @Bindable
    public Integer getSeriesRest() {
        return mSeriesRest;
    }

    public void setSeriesRest(Integer seriesRest) {
        mSeriesRest = seriesRest;
//        notifyPropertyChanged(BR.seriesRest);
    }

    @Bindable
    public Integer getRepeatsRest() {
        return mRepeatsRest;
    }

    public void setRepeatsRest(Integer repeatsRest) {
        mRepeatsRest = repeatsRest;
//        notifyPropertyChanged(BR.repeatsRest);
    }

    public int getID() {
        return mID;
    }

    @Bindable
    public Integer getSeriesRestSec() {
        return mSeriesRestSec;
    }

    public void setSeriesRestSec(Integer seriesRestSec) {
        mSeriesRestSec = seriesRestSec;
//        notifyPropertyChanged(BR.seriesRestSec);
    }

    @Bindable
    public Integer getRepeatsRestSec() {
        return mRepeatsRestSec;
    }

    public void setRepeatsRestSec(Integer repeatsRestSec) {
        mRepeatsRestSec = repeatsRestSec;
//        notifyPropertyChanged(BR.repeatsRestSec);
    }
}
