package io.fantom.trainingsession.adapters;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableInt;

import java.util.List;

import io.fantom.trainingsession.R;
import io.fantom.trainingsession.activities.MainActivity;
import io.fantom.trainingsession.activities.TrainingActivity;
import io.fantom.trainingsession.dialogs.CreateProfileDialog;
import io.fantom.trainingsession.events.DeleteProfileEvent;
import io.fantom.trainingsession.models.TrainingProfile;

public class ButtonHandler {
    private final List<TrainingProfile> trainingProfiles;
    public final ObservableInt layout = new ObservableInt(R.layout.item_profile);
    private Context mMainActivityContext = null;

    public ButtonHandler(List<TrainingProfile> profiles) {
        this.trainingProfiles = profiles;
        EventBus.getDefault().register(this);
    }

    public void setMainActivityContext(Context mainActivityContext) {
        mMainActivityContext = mainActivityContext;
    }

    /**
     * Add a user to the end of the list.
     */
    public void addToEnd() {
        if (mMainActivityContext != null) {
            CreateProfileDialog dialog = new CreateProfileDialog();
            dialog.setCreateProfileCallback(new CreateProfileDialog.CreateProfileCallback() {
                @Override
                public void getTrainingProfile(TrainingProfile profile) {
                    ButtonHandler.this.trainingProfiles.add(profile);
                }

                @Override
                public void runTrainingProfile(TrainingProfile profile) {
                    ButtonHandler.this.trainingProfiles.add(profile);
                    Intent trainActivity = new Intent(mMainActivityContext, TrainingActivity.class);
                    trainActivity.putExtra(TrainingActivity.PROFILE_ID, profile.getID());
                    mMainActivityContext.startActivity(trainActivity);
                }
            });
            dialog.show(((MainActivity) mMainActivityContext).getSupportFragmentManager(), "CreateProfile");
        }
    }

    @Subscribe
    public void delete(DeleteProfileEvent event) {
        if (!trainingProfiles.isEmpty()) {
            Optional<TrainingProfile> profileToDelete = Stream.of(trainingProfiles).filter(profile -> profile.getID() == event.getProfileID()).findFirst();
            if (profileToDelete.isPresent()){
                trainingProfiles.remove(profileToDelete.get());
            }
        }
    }
}
