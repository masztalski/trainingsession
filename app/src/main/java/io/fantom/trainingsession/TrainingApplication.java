package io.fantom.trainingsession;

import android.app.Application;

import io.fantom.trainingsession.di.ApplicationComponent;
import io.fantom.trainingsession.di.ApplicationModule;
import io.fantom.trainingsession.di.DaggerApplicationComponent;


public class TrainingApplication extends Application {
    public ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }
}
