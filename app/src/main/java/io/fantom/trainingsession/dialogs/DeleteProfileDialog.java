package io.fantom.trainingsession.dialogs;

import org.greenrobot.eventbus.EventBus;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import javax.inject.Inject;

import io.fantom.trainingsession.R;
import io.fantom.trainingsession.TrainingApplication;
import io.fantom.trainingsession.events.DeleteProfileEvent;
import io.fantom.trainingsession.repository.TrainingProfileRepository;

public class DeleteProfileDialog extends AppCompatDialogFragment {
    @Inject
    protected TrainingProfileRepository mTrainingProfileRepository;

    private int mProfileId = -1;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ((TrainingApplication) getActivity().getApplication()).mApplicationComponent.newContextComponent().inject(this);

        return new AlertDialog.Builder(getActivity(), R.style.dialogStyle)
                .setTitle("Usu�")
                .setMessage("Czy chcesz usun�� profil?")
                .setPositiveButton("Tak", (dialog, which) -> {
                    if (mProfileId != -1) mTrainingProfileRepository.delete(mProfileId);
                    EventBus.getDefault().post(new DeleteProfileEvent(mProfileId));
                    dialog.dismiss();
                })
                .setNegativeButton("Nie", (dialog, which1) -> {
                    dialog.dismiss();
                })
                .create();
    }

    public void setProfileId(int profileId) {
        mProfileId = profileId;
    }
}
