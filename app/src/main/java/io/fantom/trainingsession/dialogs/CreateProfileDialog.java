package io.fantom.trainingsession.dialogs;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;

import javax.inject.Inject;

import io.fantom.trainingsession.R;
import io.fantom.trainingsession.TrainingApplication;
import io.fantom.trainingsession.databinding.CreateProfileDialogBinding;
import io.fantom.trainingsession.models.TrainingProfile;
import io.fantom.trainingsession.repository.TrainingProfileRepository;

public class CreateProfileDialog extends AppCompatDialogFragment {
    private CreateProfileDialogBinding mBinding;

    public interface CreateProfileCallback{
        void getTrainingProfile(TrainingProfile profile);
        void runTrainingProfile(TrainingProfile profile);
    }

    @Inject
    protected TrainingProfileRepository mTrainingProfileRepository;
    private CreateProfileCallback mCreateProfileCallback;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.create_profile_dialog, null, false);
        ((TrainingApplication)getActivity().getApplication()).mApplicationComponent.newContextComponent().inject(this);

        mBinding.parent.requestFocus();

        mBinding.setProfile(new TrainingProfile());

        return new AlertDialog.Builder(getActivity(), R.style.dialogStyle)
                .setView(mBinding.getRoot())
                .setTitle("Nowy Profil")
                .setPositiveButton("Zapisz i uruchom", (dialog, which) -> {
                    Log.i("Dialog", "Zapisz i uruchom");
                    mTrainingProfileRepository.create(mBinding.getProfile());
                    if (mCreateProfileCallback != null) mCreateProfileCallback.runTrainingProfile(mBinding.getProfile());
                    dialog.dismiss();
                })
                .setNegativeButton("Anuluj", (dialog, which1) -> {
                    dialog.dismiss();
                })
                .setNeutralButton("Zapisz", (dialog, which) -> {
                    Log.i("Dialog", "Zapisz");
                    TrainingProfile profile = mBinding.getProfile();
                    mTrainingProfileRepository.create(profile);
                    if (mCreateProfileCallback != null) mCreateProfileCallback.getTrainingProfile(profile);
                    dialog.dismiss();
                })
                .create();
    }

    public void setCreateProfileCallback(CreateProfileCallback createProfileCallback) {
        mCreateProfileCallback = createProfileCallback;
    }
}
